<?php
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
<?php if ($page == 0) { ?><h1><a href="<?php print $node_url?>"><?php print $title?></a></h1><?php }; ?>
<div class="submitted"><?php if ($terms) { ?><?php print $terms?><?php }; ?><?php print $submitted?>
</div>
<div class="content clear-block"><?php print $picture?><?php print $content?></div>
<?php if ($links) { ?><div class="nodeLinks">
<?php print $links ?></div><?php }; ?>
</div>
<?php
?>
<?php $pg = $_GET['page'];
    if ( !$pg ) {
      $page = '';
    }
    else {
      $page = "?page=$pg";
    } ?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print (isset($comment->status) && $comment->status  == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; ?>">
  <div class="csubmitted">
    <?php if ($comment->new) {
    print '<a href="/node/'.$node->nid.$page.'#comment-'.$comment->cid.'"><img class="new" src="/'.$directory.'/icons/newcomment.png" title="'.t('New comment').'" /></a>'.$submitted;
    }
    else {
    print '<a href="/node/'.$node->nid.$page.'#comment-'.$comment->cid.'"><img src="/'.$directory.'/icons/comm.png" /></a>'.$submitted;
    } ?>
  </div>
  <div class="content">
    <?php if ($picture): ?>
    <?php print $picture ?>
    <?php endif; ?>
    <?php print $content ?>
   </div>
  <div class="links">
    <?php print $links; ?>
  </div>
</div>
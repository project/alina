<?php

function phptemplate_content_id($right) {
  if ($right != '') {
    $id = 'content_sidebar';
  }
  else {
    $id = 'content';
  }

  if (isset($id)) {
    print ' id="'. $id .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
   if (!empty($breadcrumb)) {
     return '<div class="breadcrumb">'. implode(' :: ', $breadcrumb) .'</div>';
   }
 }

function phptemplate_comment_submitted($comment) {
  return t('!username - !datetime',
  array(
    '!username' => theme('username', $comment),
    '!datetime' => format_date($comment->timestamp)
    ));
  }

function phptemplate_node_submitted($node) {
    return t('!username - !datetime',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}



function phptemplate_menu_item_link($link) {
if (empty($link['options'])) {
$link['options'] = array();
}

// If an item is a LOCAL TASK, render it as a tab
if ($link['type'] & MENU_IS_LOCAL_TASK) {
$link['title'] = '<span class="tab">'. check_plain($link['title']) .'</span>';
$link['options']['html'] = TRUE;
}

if (empty($link['type'])) {
$true = TRUE;
}

return l($link['title'], $link['href'], $link['options']);
}

/**
* Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
*/

function phptemplate_menu_local_tasks() {
$output = '';

if ($primary = menu_primary_local_tasks()) {
$output .= '<ul class="tabs primary clear-block">'. $primary .'</ul>';
}
if ($secondary = menu_secondary_local_tasks()) {
$output .= '<ul class="tabs secondary clear-block">'. $secondary .'</ul>';
}

return $output;
}
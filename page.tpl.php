<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <!--[if IE]>
    <link type="text/css" rel="stylesheet" href="/<?php print $directory; ?>/fix-ie.css" />
    <![endif]-->
    <?php print $scripts ?>
  </head>
  <body>
    <div id="container">
      <div id="header">
         <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
      <?php endif; ?>
       <?php if ($site_name): ?>
       <span id="site-name"><a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home"><?php print $site_name; ?></a></span>
      <?php endif; ?>
      <?php if (isset($primary_links)) : ?>
	<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      <?php endif; ?>
        <?php if (isset($secondary_links)) : ?>
          <div id="sec"><?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?></div>
        <?php endif; ?>
	<?php if ($search_box): ?>
	<?php print $search_box; ?>
	<?php endif; ?>
      </div>
      <div<?php print phptemplate_content_id($right); ?>>
        <?php if ($mission) { ?><div class="mission"><?php print $mission ?></div><?php } ?>
        <?php print $breadcrumb ?>
        <h1><?php print $title; ?></h1>
        <?php if (!empty($tabs)): ?>
        <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>
        <?php print $help; ?>
        <?php print $messages; ?>
        <?php print $content_top; ?>
        <?php print $content; ?>
	<?php print $content_bottom; ?>
	<?php if ($feed_icons): ?>
        <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>
	<?php if ($footer_message): ?>
        <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>
      </div>
      <?php if ($right): ?>
      <div id="sidebarWrapper">
        <div id="sidebar">
          <?php print $right; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>
    <div id="footer">
      <div class="FooterWrapper">
      <?php if ($footer || $footer1 || $footer2): ?>
      <div id="footer1"><?php print $footer; ?></div>
      <div id="footer2"><?php print $footer1; ?></div>
      <div id="footer3"><?php print $footer2; ?></div>
      <?php endif; ?>
      <div class="footert"><a href="http://hturkey.ru">недвижимость в турции</a></div>
      </div>
    </div>
    <?php print $closure ?>
  </body>
</html>